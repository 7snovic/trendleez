* My current environment:-
```
PHP Version: PHP 7.2.5
MySQL: mysql  Ver 14.14 Distrib 5.7.22
OS: Ubuntu 16.10
```

To run this you need to :-

* clone the repository
```bash
#change current directory to /var/www/
cd /var/www/

#clone repository
git clone git@bitbucket.org:7snovic/trendleez.git

#change directory to trendleez
cd trendleez/
```

* Create `.env` file, by copying or moving the attached `.env.eaxample` file
```bash
cp .env.example .env
```

* Edit `.env` file to point to the used database (using nano or any other editor)

* Install dependencies
```bash
composer install
```

* Generate an application key and Give the right permissions
```bash
php artisan key:generate
php artisan config:cache
chmod 777 storage -R
chmod 777 bootstrap/cache/ -R
```

* Run migrations and seeds
```bash
php artisan migrate
php artisan db:seed
```

* Setup Apache (optional)
```bash
cd /etc/apache2/sites-available/

sudo nano contact.conf
```

Add this **sample** Virtual host:-

```bash
<VirtualHost *:80>
    ServerName dev.contacts

    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/trendleez/public

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
    <Directory /var/www/trendleez/public>
            Options Indexes FollowSymLinks MultiViews
            AllowOverride All
            Order allow,deny
            allow from all
    </Directory>
</VirtualHost>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
```

Add a new host :-
```bash
sudo nano /etc/hosts
```

And add our new host to the host lists
```bash
127.0.0.1       dev.contacts
```

Enable host site and restart apache
```bash
sudo a2ensite contact.conf
sudo service apache2 restart
```

Have fun!   
now you can visit our contact manager via browser using this link http://dev.contacts/

Login credentials:  
email: admin@trendleez.com  
password: admin