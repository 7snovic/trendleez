var $dataTable = $('#datatables');
var $dth;
var $insertForm = $('#insert-form');
var $updateForm = $('#update-form');
var $insertModal = $('#insert-modal');
var $updateModal = $('#update-modal');
var $hash = window.location.hash;

function resetForm($form) {
    $('input', $form).not('[name="_token"]').val(null);
}

function modalErrors(m, es) {
    let $errors = $('.modal-errors', m);

    $errors.html('');

    for (i in es) {
        let $e = es[i];
        $errors.append($('<div>', {
            class: 'alert alert-danger',
            text: $e
        }));
    }
}

$(document).ready(function () {

    if ($hash == '#insert-modal') {
        $($hash).modal('toggle');
    }

    $insertModal.on('show.bs.modal', (ev) => {
        resetForm($(ev.currentTarget));
        $('.modal-errors').html('');
    });

    $('.insert-modal-trigger').on('click', () => {
        $insertModal.modal('toggle');
    });

    $updateModal.on('show.bs.modal', (ev) => {

        $('.modal-errors').html('');

        let $orig = $(ev.relatedTarget);

        let $target = $(ev.currentTarget);

        $('.name', $target).val($orig.data('name'));
        $('.email', $target).val($orig.data('email'));

        $('#update-form', $target).attr('action', $orig.data('path'));

    });

    var dtInitComplete = () => {
        $('.destroy').click(function (e) {
            e.preventDefault();

            let $t = $(this);

            removeRow($t.data('id'), $t.data('path'));
        });
    };

    $dth = $dataTable.DataTable({
        ajax: {
            url: $links['browse_link'],
            type: 'POST'
        },
        serverSide: true,
        processing: true,
        stateSave: true,
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        displayLength: 10,
        initComplete: dtInitComplete,
        columns: [
            {'data': 'id'},
            {'data': 'name'},
            {'data': 'email'},
            {
                data: 'id',
                render: (data, type, row) => {

                    let $editPath = $links['edit_link'] + '/' + row.id;
                    let $deletePath = $links['edit_link'] + '/' + row.id + '/destroy';

                    return `
                    <a class="btn btn-primary btn-xs"
                        data-name="${row.name}"
                        data-email="${row.email}"
                        data-path="${$editPath}"
                        data-toggle="modal" data-target="#update-modal">Edit</a>
                    <a class="btn btn-danger btn-xs destroy" data-path="${$deletePath}">Delete</a>
                    `;
                }
            },
        ]
    });

    var removeRow = function (id, path) {

        $.ajax({
            url: path,
            data: {id, id},
            method: 'POST',
            dataType: 'JSON',
            success: function (response) {
                if (
                    typeof response.status !== 'undefined' &&
                    response.status == 'OK'
                ) {
                    $dth.ajax.reload(dtInitComplete);
                } else {
                    $('#errors').show();
                    $('#errors').html(response.errors[0]);

                    setTimeout(() => {
                        $('#errors').hide('hide');
                        $('#errors').html('');
                    }, 5000);
                }
            }
        });

    };

    $('.submit-form.insert').click(function () {

        let $this = $(this);
        $this.attr('disabled', true);

        var $data = $insertForm.serialize();

        $.ajax({
            url: $insertForm.attr('action'),
            method: 'POST',
            dataType: 'JSON',
            data: $data,
            success: function (response) {
                if (
                    typeof response.status !== 'undefined' &&
                    response.status == 'OK'
                ) {
                    $dth.ajax.reload(dtInitComplete);
                    $insertModal.modal('toggle');
                } else {
                    modalErrors($insertModal, response.errors);
                }

                $this.attr('disabled', false);
            }
        });

    });

    $('.submit-form.update').click(function () {

        let $this = $(this);
        $this.attr('disabled', true);

        var $data = $updateForm.serialize();

        $.ajax({
            url: $updateForm.attr('action'),
            method: 'POST',
            dataType: 'JSON',
            data: $data,
            success: function (response) {
                if (
                    typeof response.status !== 'undefined' &&
                    response.status == 'OK'
                ) {
                    $dth.ajax.reload(dtInitComplete);
                    $updateModal.modal('toggle');
                } else {
                    modalErrors($updateModal, response.errors);
                }

                $this.attr('disabled', false);
            }
        });

    });

});
