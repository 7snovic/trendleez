<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

    Route::get('/', function () {
        return redirect('contacts');
    });

    Route::get('/home', function () {
        return redirect('contacts');
    });

    Route::group(['prefix' => 'contacts'], function () {

        Route::get('/', 'ContactsController@index')->name('contacts.browse');

        Route::post('/list', 'ContactsController@list')->name('contacts.list');

        Route::get('insert', 'ContactsController@insert')->name('contacts.insert');

        Route::post('/', 'ContactsController@store')->name('contacts.store');

        Route::post('/{id}', 'ContactsController@show')->name('contacts.show');

        Route::get('/{id}/edit', 'ContactsController@edit')->name('contacts.edit');

        Route::post('/{id}', 'ContactsController@update')->name('contacts.update');
        
        Route::post('/{id}/destroy', 'ContactsController@destroy')->name('contacts.destroy');

    });

});
