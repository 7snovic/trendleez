<?php
namespace App\Http\Controllers;

use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Contact;

class ContactsController extends Controller
{
    public function index()
    {
        return view('contacts/browse', [
            'breadcrumbs' => [
                ['text' => 'My Contact List', 'href' => route('contacts.browse')],
            ]
        ]);
    }

    public function list(Request $request)
    {
        $credentials = $request->all();

        $start = isset($credentials['start']) ? $credentials['start'] : 0;
        $limit = isset($credentials['length']) ? $credentials['length'] : 10;
        $searchFor = isset($credentials['search']) ? $credentials['search']['value'] : '';

        $contact = Contact::where('user_id', Auth::id());

        if ($searchFor != '') {
            $contact->where('name', 'LIKE', '%' . $searchFor . '%');
            $contact->orWhere('email', 'LIKE', '%' . $searchFor . '%');
        }

        $recordsFiltered = $contact->count();

        if ($limit >= 0) {
            $contact = $contact->offset($start)->limit($limit);
        }

        $contact = $contact->get();
        $recordsTotal = $contact->count();

        return response()->json([
            'data' => $contact,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
        ]);
    }

    public function store(Request $request)
    {
        $credentials = $request->all();

        $validator = Validator::make($credentials, [
            'name' => 'required|min:2|max:200',
            'email' => 'required|max:255|email',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'INVALID_CREDENTIALS',
                'errors' => $validator->errors()->toArray()
            ]);
        }

        $contact = new Contact;

        $contact->name = $credentials['name'];
        $contact->email = $credentials['email'];
        $contact->user_id = Auth::id();

        if ($contact->save()) {
            return response()->json([
                'status' => 'OK',
                'message' => ''
            ]);
        }

        return response()->json([
            'status' => 'UNEXCPEDTED_ERROR',
            'errors' => ['1']
        ]);
    }

    public function update($id, Request $request)
    {
        $credentials = $request->all();

        $validator = Validator::make($credentials, [
            'name' => 'required|min:2|max:200',
            'email' => 'required|max:255|email',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'INVALID_CREDENTIALS',
                'errors' => $validator->errors()->toArray()
            ]);
        }

        $contact = Contact::find($id);

        $contact->name = $credentials['name'];
        $contact->email = $credentials['email'];

        if ($contact->save()) {
            return response()->json([
                'status' => 'OK',
                'message' => ''
            ]);
        }

        return response()->json([
            'status' => 'UNEXCPEDTED_ERROR',
            'errors' => ['1']
        ]);
    }

    public function destroy($id, Request $request)
    {
        $validator = Validator::make(['id' => $id], [
            'id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'INVALID_CREDENTIALS',
                'errors' => $validator->errors()->toArray()
            ]);
        }

        $contact = Contact::find($id);

        if (!$contact) {
            return response()->json([
                'status' => 'INVALID_REQUEST',
                'erros' => [
                    __('errors.INVALID_REQUEST')
                ]
            ]);
        }

        if ($contact->delete()) {
            return response()->json([
                'status' => 'OK',
                'message' => ''
            ]);
        }

        return response()->json([
            'status' => 'UNEXCPEDTED_ERROR',
            'errors' => ['1']
        ]);
    }
}
