@extends('layouts.layout')

@section('headers')
<link href="{{ asset('css/dataTables/dataTables.bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('css/dataTables/dataTables.responsive.css') }}" rel="stylesheet">
@endsection
@section('scripts')

<script>
    var $links = {};
    $links['browse_link'] = '{{ route("contacts.list") }}';
    $links['edit_link'] = '{{ url("/contacts/") }}';
</script>

<script src="{{ asset('js/dataTables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('js/contacts/browse.js') }}"></script>
@endsection

@section('content')

@include('contacts/modals/insert')
@include('contacts/modals/edit')

<div class="row">
    <div class="col-lg-10">
        <h1 class="page-header">Dashboard</h1>
    </div>
    <div class="col-lg-2">
        <button class="insert-modal-trigger page-header btn btn-primary">Add Contact</button>
    </div>

    @include('layouts/breadcrumbs')
    <!-- /.col-lg-12 -->
</div>

<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                List My Contacts
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="datatable">
                    <table id="datatables"
                           class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
