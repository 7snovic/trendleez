<div class="modal fade" id="insert-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add new contact</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class='modal-errors'></div>

                <form method="POST" action="{{ route('contacts.store') }}" class="form" id="insert-form">
                    {{ csrf_field() }}
                    <div class="col-sm-12 form-group">
                        <label>Name</label>
                        <input class="form-control" name="name" type="text">
                    </div>
                    <div class="col-sm-12 form-group">
                        <label>Email</label>
                        <input class="form-control" name="email" type="text">
                    </div>
                </form>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary submit-form insert">Insert</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>