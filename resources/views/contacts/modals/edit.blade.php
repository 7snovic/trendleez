<div class="modal fade" id="update-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Contact</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class='modal-errors'></div>

                <form method="POST" action="{{ url('/contacts/') }}" class="form" id="update-form">
                    {{ csrf_field() }}
                    <div class="col-sm-12 form-group">
                        <label>Name</label>
                        <input class="form-control name" name="name" type="text">
                    </div>
                    <div class="col-sm-12 form-group">
                        <label>Email</label>
                        <input class="form-control email" name="email" type="text">
                    </div>
                </form>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary submit-form update">Update</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>